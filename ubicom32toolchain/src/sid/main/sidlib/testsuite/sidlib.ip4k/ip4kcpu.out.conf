# sid configuration file
# created by Id: configrun-sid.in,v 1.76.4.7 2001/08/07 02:13:30 fche Exp 
# run by jjohnstn @ tooth (Linux) at Wed Sep  5 15:10:35 EDT 2001
# args: --cpu ip4k --no-run --board=chiphw-gloss ip4kcpu.out
load libaudio.la audio_component_library
load libcgencpu.la cgen_component_library
load libconsoles.la console_component_library
load libgdb.la gdb_component_library
load libgloss.la gloss_component_library
load libglue.la glue_component_library
load libhd44780u.la hd44780u_component_library
load libide.la ide_component_library
load libinterrupt.la interrupt_component_library
load libloader.la loader_component_library
load libmapper.la mapper_component_library
load libmemory.la mem_component_library
load libmmu.la mmu_component_library
load libparport.la parport_component_library
load libprof.la prof_component_library
load librtc.la rtc_component_library
load libsched.la sched_component_library
load libtimers.la timer_component_library
load libuart.la uart_component_library
# first section
new hw-cpu-ip4k cpu
new hw-mapper-basic cpu-mapper
new hw-glue-sequence-8 init-sequence
new hw-glue-sequence-1 hw-reset-net
new hw-glue-sequence-8 deinit-sequence
new hw-glue-sequence-1 yield-net
new sid-sched-host-accurate host-sched
new sid-sched-sim target-sched
# ip4k timers
new hw-timer-ip4k/ref-generic timer1
new hw-timer-ip4k/ref-generic timer2
new hw-timer-ip4k/ref-generic timer3
new hw-timer-ip4k/ref-generic timer4
# gloss
new sw-gloss-generic/libgloss gloss
# gloss <-> stdio
new sid-io-stdio stdio
# ip4k harvard bus mappers
new hw-mapper-basic data-mapper
new hw-mapper-basic insn-mapper
# cpu loader
new sw-load-elf cpu-loader
# second section
# settings
set cpu step-insn-count 10000
set cpu trace-semantics? 0
set cpu trace-disassemble? 0
set cpu trace-counter? 0
set cpu trace-extract? 0
set cpu enable-warnings? 0
set host-sched num-clients 10 # large enough?
set target-sched num-clients 10 # large enough?
# pin connections
connect-pin main perform-activity -> host-sched advance
connect-pin main perform-activity -> target-sched advance
connect-pin main starting -> init-sequence input
connect-pin main stopping -> deinit-sequence input
connect-pin init-sequence output-0 -> hw-reset-net input
connect-pin hw-reset-net output-0 -> cpu reset!
connect-pin target-sched 0-event -> cpu step!
connect-pin target-sched 0-control <- cpu step-cycles
connect-pin yield-net output-0 -> cpu yield
connect-pin yield-net output-0 -> host-sched yield
# bus connections
connect-bus cpu insn-memory cpu-mapper access-port
connect-bus cpu data-memory cpu-mapper access-port
# ip4k specific .
connect-pin target-sched 0-event -> timer1 clock
connect-pin hw-reset-net output-0 -> timer1 reset
connect-pin timer1 interrupt -> cpu irq_0
connect-pin cpu tctrl_t1ie -> timer1 txie
connect-pin cpu tctrl_t1rst -> timer1 txrst
connect-pin cpu timer_enable -> timer1 timer-enable
connect-pin target-sched 0-event -> timer2 clock
connect-pin hw-reset-net output-0 -> timer2 reset
connect-pin timer2 interrupt -> cpu irq_1
connect-pin cpu tctrl_t2ie -> timer2 txie
connect-pin cpu tctrl_t2rst -> timer2 txrst
connect-pin cpu timer_enable -> timer2 timer-enable
connect-pin target-sched 0-event -> timer3 clock
connect-pin hw-reset-net output-0 -> timer3 reset
connect-pin timer3 interrupt -> cpu irq_2
connect-pin cpu tctrl_t3ie -> timer3 txie
connect-pin cpu tctrl_t3rst -> timer3 txrst
connect-pin cpu timer_enable -> timer3 timer-enable
connect-pin target-sched 0-event -> timer4 clock
connect-pin hw-reset-net output-0 -> timer4 reset
connect-pin timer4 interrupt -> cpu irq_3
connect-pin cpu tctrl_t4ie -> timer4 txie
connect-pin cpu tctrl_t4rst -> timer4 txrst
connect-pin cpu timer_enable -> timer4 timer-enable
# gloss
relate gloss cpu cpu
connect-pin init-sequence output-2 -> gloss reset
connect-pin cpu trap <-> gloss trap
connect-pin cpu trap-code -> gloss trap-code
set gloss verbose? 0
connect-bus gloss target-memory cpu-mapper access-port
# gloss <-> stdio
set host-sched 0-regular? 1
set host-sched 0-time 150 # apprx. human perception limit
connect-pin host-sched 0-event -> stdio poll
connect-pin gloss debug-tx -> stdio stdout
connect-pin gloss debug-rx <- stdio stdin
# gloss w/o gdb
connect-pin gloss process-signal -> main stop!
connect-pin gloss process-signal -> yield-net input
# ip4k harvard bus specific.
disconnect-bus cpu insn-memory cpu-mapper access-port
connect-bus cpu insn-memory insn-mapper access-port
disconnect-bus cpu data-memory cpu-mapper access-port
# ip4k harvard bus data mapping.
connect-bus cpu data-memory data-mapper access-port
# ip4k harvard bus specific gloss .
disconnect-bus gloss target-memory cpu-mapper access-port
connect-bus gloss target-memory data-mapper access-port
# ip4k specific gloss .
disconnect-bus gloss target-memory data-mapper access-port
connect-bus gloss target-memory cpu data-bus
set cpu engine-type pbb
# cpu loader
set cpu-loader file "ip4kcpu.out"
set cpu-loader verbose? 0
connect-bus cpu-loader load-accessor-data cpu debugger-bus
connect-bus cpu-loader load-accessor-insn cpu-mapper access-port # don't trace loading
connect-pin init-sequence output-1 -> cpu-loader load!
connect-pin cpu-loader start-pc-set -> cpu start-pc-set!
connect-pin cpu-loader endian-set -> cpu endian-set!
connect-pin cpu-loader error -> main stop!
# ip4k memory
new hw-memory-ram/rom-basic flashmem1
set flashmem1 size 0x20000
new hw-memory-ram/rom-basic flashmem2
set flashmem2 size 0x20000
new hw-memory-ram/rom-basic flashmem3
set flashmem3 size 0x20000
new hw-memory-ram/rom-basic flashmem4
set flashmem4 size 0x20000
new hw-memory-ram/rom-basic flashmem5
set flashmem5 size 0x20000
new hw-memory-ram/rom-basic flashmem6
set flashmem6 size 0x20000
new hw-memory-ram/rom-basic flashmem7
set flashmem7 size 0x20000
new hw-memory-ram/rom-basic flashmem8
set flashmem8 size 0x20000
new hw-memory-ram/rom-basic sram
set sram size 0x10000
new hw-memory-ram/rom-basic flashinfo
set sram size 0x2048
new hw-memory-ram/rom-basic extprog
set extprog size 0x600000
new hw-memory-ram/rom-basic hrttable1
set hrttable1 size 0x100
new hw-memory-ram/rom-basic hrttable2
set hrttable2 size 0x100
new hw-memory-ram/rom-basic tracetable
set tracetable size 0x1000
new hw-memory-ram/rom-basic sramdata
set sramdata size 0x10000
new hw-memory-ram/rom-basic extdata
set extdata size 0xb00000
# ip4k memory mapping .
connect-bus insn-mapper flashmem1:[0,0x01ffff] flashmem1 read-write-port
connect-bus insn-mapper flashmem2:[0x100000,0x11ffff] flashmem2 read-write-port
connect-bus insn-mapper flashmem3:[0x200000,0x21ffff] flashmem3 read-write-port
connect-bus insn-mapper flashmem4:[0x300000,0x31ffff] flashmem4 read-write-port
connect-bus insn-mapper flashmem5:[0x400000,0x41ffff] flashmem5 read-write-port
connect-bus insn-mapper flashmem6:[0x500000,0x51ffff] flashmem6 read-write-port
connect-bus insn-mapper flashmem7:[0x600000,0x61ffff] flashmem7 read-write-port
connect-bus insn-mapper flashmem8:[0x700000,0x71ffff] flashmem8 read-write-port
connect-bus insn-mapper sram:[0x800000,0x80ffff] sram read-write-port
connect-bus insn-mapper flashinfo:[0x900000,0x9007ff] flashinfo read-write-port
connect-bus insn-mapper extprog:[0xa00000,0xffffff] extprog read-write-port
connect-bus data-mapper timer1:[0x290,0x29f] timer1 registers
connect-bus data-mapper timer2:[0x2a0,0x2af] timer2 registers
connect-bus data-mapper timer3:[0x2b0,0x2bf] timer3 registers
connect-bus data-mapper timer4:[0x2c0,0x2cf] timer4 registers
connect-bus data-mapper hrttable1:[0x800,0x8ff] hrttable1 read-write-port
connect-bus data-mapper hrttable2:[0x900,0x9ff] hrttable2 read-write-port
connect-bus data-mapper tracetable:[0x4000,0x4fff] tracetable read-write-port
connect-bus data-mapper sramdata:[0x100000,0x10ffff] sramdata read-write-port
connect-bus data-mapper extdata:[0x500000,0xffffff] extdata read-write-port
set timer1 tick-count 10000
set timer2 tick-count 10000
set timer3 tick-count 10000
set timer4 tick-count 10000
# ip4k syscall enablement .
set cpu syscall-trap 63
# ip4k harvard-bus mapping .
connect-bus cpu-mapper data[0x1000000,0x1ffffff] data-mapper access-port
connect-bus cpu-mapper insn[0x2000000,0x2ffffff] insn-mapper access-port
# memory region 1 (0x00000400,0x00800000)
new hw-memory-ram/rom-basic mem1
set mem1 size 8388608
connect-bus cpu-mapper mem1:[1024,8389631] mem1 read-write-port
