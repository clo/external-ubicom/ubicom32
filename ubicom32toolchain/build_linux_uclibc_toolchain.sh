#!/bin/bash

######################################################################
#  Build script for ubicom32 toolchain (-linux-uclibc target)
#
######################################################################

# Source common variables and functions
. common.sh

# Source config for the build
. config.build

######################################################################
#
# main script starts here
#

# private variables
SET_DATE=
CLEAN_UP_DATE=

# Process command line options
process_cmd_options
KERNEL_SRC=$CONFIG_KERNEL_SRC
UCLIBC_SRC=$CONFIG_UCLIBC_SRC

Status "Configuration Parameters"
Info "Sources from  = $SRCDIR"
Info "Build using   = $BLDDIR"
Info "Release to    = $RELDIR"
Info "uClibc dir    = $UCLIBC_SRC"
Info "Kernel source = $KERNEL_SRC"

# always use elf toolchain
CROSS_COMPILE=$RELDIR/bin/ubicom32-elf-

# Info Fixing broken cvs install scripts
find $SRCDIR -name install-sh | xargs chmod +x

if [ "$CONFIG_SET_DATE" == "y" ]; then
    SET_DATE=$(date +%Y%m%d)
    CLEAN_UP_DATE="$SRCDIR/gcc/gcc/DATESTAMP.build_all_sav"
    mv $SRCDIR/gcc/gcc/DATESTAMP $CLEAN_UP_DATE
    echo $SET_DATE
 > $SRCDIR/gcc/gcc/DATESTAMP
fi

[ -z $SET_DATE ] || \
Info "Date	= $SET_DATE"

export LD_LIBRARY_PATH="$RELDIR/lib${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}"

######################################################################
# Configure and build uClinux/uClibc if needed

if [ ! -e "$UCLIBC_SRC" ]; then
    error_and_exit "$UCLIBC_SRC not found";
fi

if [ ! -e "$KERNEL_SRC" ]; then
    error_and_exit "$KERNEL_SRC not found";
fi

######################################################################
set_target elf

Status "Build Kernel Headers"
rm $BLDDIR/$KERNEL_HDR -rf
set_dir $KERNEL_HDR
mkdir -p usr
$MAKE -j1 -C $KERNEL_SRC \
    INSTALL_HDR_PATH="$BLDDIR/$KERNEL_HDR/usr" \
    KBUILD_OUTPUT="$BLDDIR/$KERNEL_HDR" \
    ARCH="$TARGET_ARCH" CROSS_COMPILE="$CROSS_COMPILE" \
    headers_check headers_install > build.txt \
    || error_and_exit "Error Building Kernel Headers"
# remove the headers check left overs
find usr -type f -name '.*' -print0 | xargs -0 rm -fv >> build.txt

######################################################################
set_target linux-uclibc
EXTRA=
UCLIBC_CFLAGS="-g -O2 -mtls"
build_and_install_uClibc "" -DUBICOM32_ARCH_VERSION=5
    
mkdir -p $RELDIR/bin
Status "Copy host utils $RELDIR/bin"
make_uClibc hostutils > log_/hostutils_log_.txt
cp -v "$UCLIBC_SRC/utils/ldd.host" "$RELDIR/bin/$TARGET-ldd" \
    || error_and_exit "Error Copying $TARGET-ldd"
cp -v "$UCLIBC_SRC/utils/ldconfig.host" \
    "$RELDIR/bin/$TARGET-ldconfig" \
    || error_and_exit "Error Copying $TARGET-ldconfig"
    
build_and_install_uClibc_v5 march-ubicom32v5 -march=ubicom32v5 \
    -DUBICOM32_ARCH_VERSION=5

Status "Copy kernel headers to $CROSS_RUNTIME"
set_dir .
cp -rv "$BLDDIR/$KERNEL_HDR/usr" $CROSS_RUNTIME > khdr_copy.txt \
    || error_and_exit "Error Copying Kernel Headers"
EXTRA=--with-sysroot=$CROSS_RUNTIME

# now build the c,c++ toolchain

configure_and_build_toolchain linux $CONFIG_BUILD_LINUX_LANGUAGES \
    --enable-threads=posix \
    --disable-newlib \
    --disable-libgloss \
    $EXTRA

Status "Install shared libs"

install_target_libs libstdc++
install_target_libs libgcc_s
install_target_libs libmudflap
install_target_libs libmudflapth

# build gdbserver
configure_and_build gdbserver $TARGET/gdb/gdbserver \
    $SRCDIR/gdb/gdbserver/configure \
    --prefix="$CROSS_RUNTIME" \
    --host=$TARGET \
    CC=$RELDIR/bin/$TARGET-gcc \
    CFLAGS="-static -O2" LDFLAGS="-s"

######################################################################
Status "BUILD COMPLETE"
clean_up
######################################################################
