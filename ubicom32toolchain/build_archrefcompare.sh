#!/bin/bash

PWD=`pwd`
BUILD=$PWD/build/archrefcompare
SOURCE=$PWD/src/archrefcompare
INSTALL=$PWD/release/archrefcompare
LOG=$BUILD/log

ARCH=$(uname -m)
if [ "$ARCH" = "i686" ]; then 
  echo "Cannot build QEMU-Archrefcompare on 32-bit system"
  exit 1
fi

echo ""
echo "Removing $BUILD"
echo "         $LOG"
rm -rf $BUILD $LOG

mkdir -p $BUILD $LOG

echo " "
echo "======================="
echo " "
echo "  Building QEMU"
echo " "
echo "  SOURCE=$SOURCE"
echo "  INSTALL=$INSTALL"
echo "  LOG=$LOG"
echo " "
echo -n "  "
date
echo " "
echo "  Configuring QEMU"
cd $BUILD
$SOURCE/configure --prefix=$INSTALL 	\
		  --disable-curl 	\
		  --disable-bluez	\
		  --disable-vnc-tls 	\
		  --enable-debug	\
		  --target-list="ubicom32-softmmu ubicom32el-softmmu" \
			> $LOG/configure.log 2>&1 
rc=$?
if [ $rc -ne 0 ]; then echo "rc = $rc"; exit; fi

echo "  Building QEMU"
make V=1 CFLAGS=-g > $LOG/build.log 2>&1
rc=$?
if [ $rc -ne 0 ]; then echo "rc = $rc"; exit; fi

echo "  Installing QEMU"
make V=1 CFLAGS=-g install > $LOG/install.log 2>&1
rc=$?
if [ $rc -ne 0 ]; then echo "rc = $rc"; exit; fi

echo " "
echo "  QEMU build completed"
echo -n "  "
date
echo " "
echo "======================="
