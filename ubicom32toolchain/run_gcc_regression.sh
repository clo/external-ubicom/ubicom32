#!/bin/bash


echo ""
echo "Run GCC Regression Test for Ubicom32"
echo ""
date
echo ""

# Source common variables and functions
. ./common.sh

# Source config for the build
. ./config.build

CONFIG=${1:-ubicom32v5be}
shift

OPT=${1:-execute.exp}

case "$CONFIG" in
  ubicom32v5 | ubicom32v5be)
    QEMU=qemu-system-ubicom32
    ARCH=ubicom32v5
    ENDIAN=mbig-endian
    MAPFILE=ip8k.mapfile
    ;;
  ubicom32v5le)
    QEMU=qemu-system-ubicom32el
    ARCH=ubicom32v5
    ENDIAN=mlittle-endian
    MAPFILE=ip8k.mapfile
    ;;
  ubicom32v6 | ubicom32v6le)
    QEMU=qemu-system-ubicom32el
    ARCH=ubicom32v6
    ENDIAN=mlittle-endian
    MAPFILE=akronite.mapfile
    ;;
  ubicom32v6be)
    QEMU=qemu-system-ubicom32
    ARCH=ubicom32v6
    ENDIAN=mbig-endian
    MAPFILE=akronite.mapfile
    ;;
  *) 
    echo "Invalid architecture: $CONFIG"
    exit 1
    ;;
esac

TODAY=`date "+%Y_%m_%d"`
RESULTS=$RESULTSDIR/$TODAY-$ARCH-$ENDIAN
GCC=$RELDIR/bin/ubicom32-elf-gcc
DEJADIR=$SRCDIR/dejagnu
TARGET_BOARD=ubicom32-qemu
LDSCRIPT=$SRCDIR/newlib/libgloss/ubicom32/ubicom32-qemu.ld
MAPFILE=$SRCDIR/qemu/target-ubicom32/$MAPFILE

echo "Removing test directory $TESTDIR"
rm -rf $TESTDIR
mkdir -p $TESTDIR

echo "Creating results directory $RESULTS"
rm -rf $RESULTS
mkdir -p $RESULTS

pushd $TESTDIR

# Set up test parameters
cat > target_config.exp <<EOF
set target_config_cflags  "-march=$ARCH -$ENDIAN -msemihosting"
set target_config_ldflags "-T$LDSCRIPT"
set target_config_sim     "$QEMU -nographic -monitor null -serial null -semihosting -readconfig $MAPFILE -kernel"
set tool_timeout 60
EOF

cat > site.exp<<EOF
set rootme ""
set srcdir "$ROOTDIR"
set host_triplet x86_64-unknown-linux-gnu
set build_triplet x86_64-unknown-linux-gnu
set target_triplet ubicom32-unknown-elf
set target_alias ubicom32-elf
set libiconv ""
set CFLAGS ""
set CXXFLAGS ""
set TESTING_IN_BUILD_TREE 1
set HAVE_LIBSTDCXX_V3 1
set tmpdir "$TESTDIR"
set srcdir "$SRCDIR/gcc/gcc/testsuite"
if ![info exists boards_dir] {
    set boards_dir "$DEJADIR/baseboards"
}
lappend boards_dir "$DEJADIR/baseboards"
EOF

touch $RESULTS/gcc.start

runtest --tool gcc --tool_exec $GCC --target_board=$TARGET_BOARD -v -v \
	$OPT 2>&1 | tee runtest.log

touch $RESULTS/gcc.stop

cp gcc.sum $RESULTS
cp gcc.log $RESULTS

popd
